"""
T lines -> 4
======
N x M values = Answer
1   1           R
2   2           L
3   1           D
3   3           R
"""


class Test():
    def __init__(self):
        pass

    def input(self, value):
        loop = value
        list_content = []
        while loop >= 1:
            n, m = input("Enter the value N and M:\n").split()
            list_content.append(self.output(int(n), int(m)))
            loop -= 1
        else:
            print(list_content)

    def output(self, n, m):
        value = ""
        if n == m:
            if n == 0 or m == 0:
                value = "Error"
            elif n % 2 == 0:
                value = "L"
            else:
                value = "R"
        else:
            value = "D"

        return value


call = Test()

number = int(input("T lines\n"))
call.input(number)
